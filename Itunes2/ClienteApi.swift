//
//  ClienteApi.swift
//  Itunes2
//
//  Created by Jose Antonio Aravena on 13-12-19.
//  Copyright © 2019 Jose Antonio Aravena. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class ClienteApi {

    var total=0
    func getSongs(_ busqueda: String, completionHandler: @escaping (_ resultado: Result) -> Void ) {
        Alamofire.request("https://itunes.apple.com/search?term=\(busqueda)&mediaType=music&entity=song&limit=50")
            .responseJSON { response in
            print("Result: \(response.result)")
            if response.result.isFailure {
                return
            }

            if let data = response.data {
                guard let result = try? JSONDecoder().decode(Result.self, from: data) else {
                    print("Error: No se puede decodificar JSON")
                    return
                }
                print("numero de resultados: \(result.total)")
                print("tracks:")
                for track in result.canciones {
                    print("- \(track.titulo)")
                    print("- \(track.arte.lastPathComponent)")
                    print("- \(track.banda)")
                    print("- \(track.trackId)")
                    print("- \(track.albumId)")
                    print("")
                }
                completionHandler(result)
            }
        }
    }

    func getAlbum(_ busqueda: String, album: Int, completionHandler: @escaping (_ resultado: [Track]) -> Void ) {
        Alamofire.request("https://itunes.apple.com/search?term=\(busqueda)&mediaType=music&entity=song&limit=50")
            .responseJSON { response in
            print("Result: \(response.result)")
            if response.result.isFailure {
                return
            }
            if let data = response.data {

                guard let result = try? JSONDecoder().decode(Result.self, from: data) else {
                    print("Error: No se puede decodificar JSON")
                    return
                }
                print("numero de resultados: \(result.total)")
                print("tracks:")

                let tracks = result.canciones.filter({ $0.albumId == album})
                    .sorted(by: { $0.trackNumber < $1.trackNumber})
                for track in tracks {
                    print("- \(track.titulo)")
                    print("- \(track.arte.lastPathComponent)")
                    print("- \(track.banda)")
                    print("- \(track.trackId)")
                    print("- \(track.albumId)")
                    print("")
                }
                completionHandler(tracks)
            }
        }
    }

    func getImage(arte: URL, completionHandler: @escaping (UIImage) -> Void) {
        Alamofire.request(arte).responseImage { response in
            debugPrint(response)
            print(response.request!)
            print(response.response!)
            debugPrint(response.result)
            if let image = response.result.value {
                print("image downloaded: \(image)")
                completionHandler(image)
            }
        }
    }

    func descargarTrack(track: Track, completionHandler:@escaping (URL) -> Void) {
        let ext = track.previewUrl.pathExtension
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let ruta = documentsURL.appendingPathComponent(track.titulo + ext)
        let destino: DownloadRequest.DownloadFileDestination = { _, _ in
            return (ruta, [.removePreviousFile, .createIntermediateDirectories])
        }

         Alamofire.download(track.previewUrl, to: destino)
            .downloadProgress { (progreso) in
                print((String)((Int)(progreso.fractionCompleted*100))+"%")
            }
            .responseData { _ in
            completionHandler(ruta)
        }
    }
}
