//
//  AppDelegate.swift
//  Itunes2
//
//  Created by Jose Antonio Aravena on 13-12-19.
//  Copyright © 2019 Jose Antonio Aravena. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

 var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
