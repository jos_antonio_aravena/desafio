//
//  BandPresenter.swift
//  Itunes2
//
//  Created by Jose Antonio Aravena on 13-12-19.
//  Copyright © 2019 Jose Antonio Aravena. All rights reserved.
//

import Foundation

protocol BandView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setBand(_ bands: [Track])
    func setEmptyBand()
}

class BandPresenter {
    fileprivate let bandService: ClienteApi
    weak fileprivate var bandView: BandViewController?

    init(bandService: ClienteApi) {
        self.bandService = bandService
    }

    func attachView(_ view: BandViewController) {
        bandView = view
    }

    func detachView() {
        bandView = nil
    }

    func getTracks(album: String, idAlbum: Int, completion:  @escaping ([Track]) -> Void ) {
        //self.bandView?.startLoading()
        bandService.getAlbum(album, album: idAlbum, completionHandler: completion )
    }
}
