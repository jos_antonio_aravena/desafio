//
//  SinglePresenter.swift
//  Itunes2
//
//  Created by Jose Antonio Aravena on 13-12-19.
//  Copyright © 2019 Jose Antonio Aravena. All rights reserved.
//
import Foundation

protocol SingleView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setEmptyBand()
}

class SinglePresenter {
    fileprivate let singleService: ClienteApi
    weak fileprivate var singleView: MainViewController?

    init(singleService: ClienteApi) {
        self.singleService = singleService
    }

    func attachView(_ view: MainViewController) {
        singleView = view
    }

    func detachView() {
        singleView = nil
    }

    func getSingles(busqueda: String, completion:  @escaping (Result) -> Void ) {
        singleService.getSongs(busqueda, completionHandler: completion)
    }
}
