//
//  Result.swift
//  Itunes
//
//  Created by Jose Antonio Aravena on 13-12-19.
//  Copyright © 2019 Jose Antonio Aravena. All rights reserved.
//

import Foundation

struct Result: Decodable {
    let total: Int
    let canciones: [Track]

    enum CodingKeys: String, CodingKey {
        case total = "resultCount"
        case canciones = "results"
    }
}
