//
//  Track.swift
//  Itunes
//
//  Created by Jose Antonio Aravena on 13-12-19.
//  Copyright © 2019 Jose Antonio Aravena. All rights reserved.
//

import Foundation

struct Track: Decodable {
    let trackId: Int
    let arte: URL
    let previewUrl: URL
    let banda: String
    let titulo: String
    let albumId: Int
    let album: String
    let trackCount: Int
    let trackNumber: Int
    let kind: String

    enum CodingKeys: String, CodingKey {
        case trackId
        case arte = "artworkUrl100"
        case previewUrl
        case banda = "artistName"
        case titulo = "trackName"
        case albumId = "collectionId"
        case album = "collectionName"
        case trackCount
        case trackNumber
        case kind
    }
}
