//
//  BandViewController.swift
//  Itunes2
//
//  Created by Jose Antonio Aravena on 13-12-19.
//  Copyright © 2019 Jose Antonio Aravena. All rights reserved.
//

import UIKit
import AVFoundation

class BandViewController: UIViewController {

    @IBOutlet weak var imagenBanda: UIImageView!
    @IBOutlet weak var banda: UILabel!
    @IBOutlet weak var tablaDeCanciones: UITableView!
    var total=0
    var resultados: [Track] = []
    var audioPlayer: AVAudioPlayer?
    var album="Californication"
    var albumid=945575406
    fileprivate let bandPresenter = BandPresenter(bandService: ClienteApi())
    override func viewDidLoad() {
        super.viewDidLoad()

        bandPresenter.attachView(self)

        tablaDeCanciones.delegate=self
        tablaDeCanciones.dataSource=self
        tablaDeCanciones.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")

        let busqueda = album.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        bandPresenter.getTracks(album: busqueda, idAlbum: albumid, completion: completion)
        print(busqueda)
    }

    func completion(tracks: [Track]) {
        self.resultados = tracks
        self.total = tracks.count
        if self.total > 0 {
            //self.tituloAlbum.title=tracks[0].album
            self.banda.text=tracks[0].banda
            ClienteApi().getImage(arte: tracks[0].arte, completionHandler: { image in
                self.imagenBanda.image = image
            })
            self.tablaDeCanciones.reloadData()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.audioPlayer?.stop()
    }
}

extension BandViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let track = self.resultados[indexPath.row]
        cell.textLabel!.text = track.titulo
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}

extension BandViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let track = resultados[indexPath.row]
        print(track)
        ClienteApi().descargarTrack(track: track, completionHandler: { ruta in
            do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: ruta)
            self.audioPlayer?.play()
            } catch {
            }
            })
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
