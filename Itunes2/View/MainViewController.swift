//
//  MainViewController.swift
//  Itunes2
//
//  Created by Jose Antonio Aravena on 13-12-19.
//  Copyright © 2019 Jose Antonio Aravena. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    var resultados: [Track] = []
    var total: Int = 0
    var track0: Track?
    let tabla = UITableView()
    fileprivate let singlePresenter = SinglePresenter(singleService: ClienteApi())
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
        setupTableView()
        singlePresenter.attachView(self)
        tabla.delegate=self
        tabla.dataSource=self
        searchBar.delegate=self
        self.tabla.reloadData()
    }
    func setupSearchBar() {
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0.0).isActive = true
        searchBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0.0).isActive = true
        searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0.0).isActive = true
    }
    func setupTableView() {
        view.addSubview(tabla)
        tabla.translatesAutoresizingMaskIntoConstraints = false
        tabla.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
        tabla.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tabla.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tabla.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tabla.register(UINib(nibName: "CancionCell", bundle: nil), forCellReuseIdentifier: "CancionCell")
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CancionCell")
            as? CancionCell else {
            return UITableViewCell()
        }
        let track = self.resultados[indexPath.row]
        cell.banda.text = track.banda
        cell.titulo.text = track.titulo
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return min(self.total, 20)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let track = resultados[indexPath.row]
        print(track)
        self.track0=track
        tableView.deselectRow(at: indexPath, animated: true)
        let bandViewController = BandViewController()
        bandViewController.album = track0!.album
        bandViewController.albumid = Int(track0!.albumId)
        self.navigationController?.pushViewController(bandViewController, animated: true)
    }
}

extension MainViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !searchBar.text!.isEmpty {
            searchBar.resignFirstResponder()
            let busqueda: String = searchBar.text!
                .addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
            singlePresenter.getSingles(busqueda: busqueda, completion: { result in
                self.resultados = result.canciones
                self.total = result.total
                self.tabla.reloadData()
            })
        }
    }
}
